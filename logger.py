#!/usr/bin/env python3
# make sure to `sudo chmod 666 /dev/ttyACM0`

import sys
import serial
import time
import csv
import select
import tty
import termios
import ast

def twos_comp(val, bits):
    if (val & (1 << (bits - 1))) != 0:
        val = val - (1 << bits)
    return val


def isKeypress():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])


class SerialStream:

    def __init__(self, serial_port):
        self.serial_port = serial_port
        self.run_stream = True

    def data_stream(self):
        while self.run_stream:
            s = self.serial_port.readline().decode("utf-8")
            data = "Empty"
            try:
                data = ast.literal_eval(s)
            except Exception as e:
                print("Could not convert string. Error: {}".format(e))
                print("Retrying \n")
            yield data


class Process:

    R_ref = 5000 * 2
    AD7794_resolution = 24
    AD7794_gain = 4
    R0 = 1000
    temp_coef = 3.85e-3

    HX771_resolution = 24
    load_cell_scale = 5

    offset1 = -86454
    offset2 = -585
    offset3 = -33016
    offset4 = -4369

    calibration_weight = 0.4587

    calibration_val1 = 194814
    calibration_val2 = 196807
    calibration_val3 = 195956
    calibration_val4 = 197554

    old_eltemp1 = 0
    old_eltemp2 = 0

    def process_data(self, sensors, distance):
        now = time.time()
        temperature = sensors["Temp"] / 1000
        humidity = sensors["hum"] / 1000
        V1 = sensors["V1"] / 1000 * ((4700 + 1000) / 2000)
        V2 = sensors["V2"] / 2000000

        V1 = V1 - V2
        eltemp = (sensors["elTemp1"] * self.R_ref) / (2**self.AD7794_resolution * self.AD7794_gain)
        eltemp = (eltemp - self.R0) / (self.R0 * self.temp_coef)

        if eltemp < -100 or eltemp > 400:
            eltemp = self.old_eltemp1
        self.old_eltemp1 = eltemp

        eltemp1 = (sensors["elTemp2"] * self.R_ref) / (2**self.AD7794_resolution * self.AD7794_gain)
        eltemp1 = (eltemp1 - self.R0) / (self.R0 * self.temp_coef)

        if eltemp1 < -100 or eltemp1 > 400:
            eltemp1 = self.old_eltemp2
        self.old_eltemp2 = eltemp1

        # load1 = twos_comp(int(sensors["load1"]), 24) / (2**(self.HX771_resolution - 1)) * self.load_cell_scale
        # load2 = twos_comp(int(sensors["load2"]), 24) / (2**(self.HX771_resolution - 1)) * self.load_cell_scale
        # load3 = twos_comp(int(sensors["load3"]), 24) / (2**(self.HX771_resolution - 1)) * self.load_cell_scale
        # load4 = twos_comp(int(sensors["load4"]), 24) / (2**(self.HX771_resolution - 1)) * self.load_cell_scale

        load1 = (twos_comp(int(sensors["load1"]), 24) - self.offset1) / self.calibration_val1 * self.calibration_weight - 1.02565295512643
        load2 = (twos_comp(int(sensors["load2"]), 24) - self.offset2) / self.calibration_val2 * self.calibration_weight - -0.143007690783356
        load3 = (twos_comp(int(sensors["load3"]), 24) - self.offset3) / self.calibration_val3 * self.calibration_weight - -1.09910939496622
        load4 = (twos_comp(int(sensors["load4"]), 24) - self.offset4) / self.calibration_val4 * self.calibration_weight - 0.256462788908349

        #print("1: " + str(load1) + "2: " + str(load2) + "3: " + str(load3) + "4: " + str(load4))
        return [now, temperature, humidity, V1, V2, load1, load2, load3,
                load4, eltemp, eltemp1, distance]


def main(argv):
    try:
        serial_port = serial.Serial(port='/dev/' + argv, baudrate=115200, dsrdtr=False)
        serial_port.timeout = 5
        print(serial_port.name)
    except Exception as e:
        print("Could not open serial port Error: {}".format(e))
        serial_port = None

    csv_file = open("data.csv", "w", newline="")
    old_settings = termios.tcgetattr(sys.stdin)
    stream = SerialStream(serial_port)
    process = Process()

    c = "0"

    if serial_port.isOpen():
        tty.setcbreak(sys.stdin.fileno())
        serial_port.flushInput()
        serial_port.flushOutput()

        stream_data = stream.data_stream()
        writer = csv.writer(csv_file)

        writer.writerow(['Time', 'EnvironmentTemperature', 'RelativeHumidity', 'Voltage', 'Current', 'Force1', 'Force2', 'Force3', 'Force4', 'ElectrodeTemperature1', 'ElectrodeTemperature2', 'Distance'])
        try:
            for row in stream_data:
                if isKeypress():
                    userInput = sys.stdin.readline()

                    if '\x1b' in userInput:  # x1b is ESC
                        break
                    try:
                        c = int(userInput)
                        print(" -- Distance: " + userInput + "\n")
                    except:
                        print("Enter a valid number!")

                if row != "Empty":

                    data = process.process_data(row, c)
                    sys.stdout.write("\033[F\033[FRoom Temperature: {0:2.2f}°C, Humidity: {1:2.2f}%rh, Power: {2:2.4f}W".format(
                          data[1], data[2], data[3] * data[4]))
                    sys.stdout.write("\nElectrode 1: {0:2.4f}°C, Electrode 2: {1:2.4f}°C        ".format(data[9], data[10]))
                    sys.stdout.write("\nLoad: {0:2.4f}kg --- Loads [{1:2.4f}kg, {2:2.4f}kg, {3:2.4f}kg, {4:2.4f}kg]".format(data[5] + data[6] + data[7] + data[8], data[5], data[6], data[7], data[8]))
                    sys.stdout.flush()
                    writer.writerow(data)

        except Exception as e:
            print("error communicating...: {}".format(e))
        finally:
            csv_file.close()
            serial_port.close()
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

    csv_file.close()
    serial_port.close()
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

if __name__ == "__main__":
    main(sys.argv[1])
