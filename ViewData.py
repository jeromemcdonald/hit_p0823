import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

name = input("file name (eg. data1): ")

data = np.genfromtxt(name + '.csv', dtype=float, delimiter=',', names=['Time', 'EnvironmentTemperature', 'RelativeHumidity', 'Voltage', 'Current', 'Force1', 'Force2', 'Force3', 'Force4', 'ElectrodeTemperature1', 'ElectrodeTemperature2', 'Distance'])

t = data['Time']
temp_env = data['EnvironmentTemperature']
humidity_env = data['RelativeHumidity']

voltage = data['Voltage']
current = data['Current']

power = voltage * current

force1 = data['Force1']
force2 = data['Force2']
force3 = data['Force3']
force4 = data['Force4']

El_temp1 = data['ElectrodeTemperature1']
El_temp2 = data['ElectrodeTemperature2']

fig, ax = plt.subplots(nrows=2, ncols=2)

ax[0, 0].plot(t, temp_env, t, humidity_env)
ax[0, 0].legend(["Temperature", "Humidity"])
ax[0, 0].set_xlabel('Time (seconds)')
ax[0, 0].set_ylabel('Temperature (°C) / Humidity (%rh)')
ax[0, 0].set_title('Environment temperature and humidity')

ax[1, 0].plot(t, voltage, t, current, t, power)
ax[1, 0].set_xlabel('Time (seconds)')
ax[1, 0].set_ylabel('Voltage (V), Current (A), Power (W), Res (Ohm)')
ax[1, 0].set_title('Power dissipation over electrodes')

ax[0, 1].plot(t, El_temp1, t, El_temp2)
ax[0, 1].legend(["Electrode 1", "Electrode 2"])
ax[0, 1].set_xlabel('Time (seconds)')
ax[0, 1].set_ylabel('Temperature (°C)')
ax[0, 1].set_title('Electrode temperature Pt1000')

ax[1, 1].plot(t, force1 + force2 + force3 + force4)
ax[1, 1].set_xlabel('Time (seconds)')
ax[1, 1].set_ylabel('Weight (kg)')
ax[1, 1].set_title('Load cells measured total weight')

fig.tight_layout()
plt.show()
