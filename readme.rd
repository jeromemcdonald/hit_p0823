Plug in

Open Terminal with right klick, Open Terminal in the working folder
Enable port:

  `sudo chmod 666 /dev/ttyACM0`

Password for sudo: lab
If ACM0 is not available, try ACM1

Run Script:

  `python3 logger.py ttyACM0`
  
Rename data.csv and copy to working directory
Run Visualisation script:

  `python3 ViewData.py`
  
  Enter file name (Without .csv)
